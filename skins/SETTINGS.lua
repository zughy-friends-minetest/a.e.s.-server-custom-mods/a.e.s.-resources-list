-- skins unlocked by default
collectible_skins.SETTINGS.default_skins = {
    "default_1",
    "default_2",
    "default_3",
    "default_4",
    "default_5",
    "default_6"
    }

-- only for retrocompatibility purpose. Ignore this setting if you haven't ever
-- used this mod before. If you did, list the new technical_names of your skins
-- following the old IDs order
collectible_skins.SETTINGS.migrate_skins = {
    "default_1",
    "default_2",
    "default_3",
    "default_4",
    "default_5",
    "default_6",
    "hub_faithconnor",
    "hub_pigeon",
    "hub_skelegrotto",
    "hub_laracraft"
}
