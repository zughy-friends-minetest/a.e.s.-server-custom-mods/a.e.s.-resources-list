-- spawn point
hub.settings.hub_spawn_point = vector.new(6, 2034, 170)

-- custom viewing rotation in degrees when joining the server, to force a certain direction
hub.settings.join_rotation_x = 0

-- if players in the hub go lower than this coordinate, they get teleported back to the spawn point
hub.settings.y_limit = 1850

-- music to reproduce whilst inside the hub
-- `params` are for tweaking the track, as in Minetest sound parameter tables (e.g. gain = 1.2). Some won't work (e.g. exclude_player)
--[[hub.settings.music = {
  track = "caketown",
  description = "Caketown - soothing and joyful music for the hub",
  params = {}
 }]]
hub.settings.music = {}

-- hotbar additional items
hub.settings.hotbar_items = {
  "",
  "",
  "magic_compass:compass",
  "",
  "collectible_skins:wardrobe",
  "achievements_lib:achievements_menu",
  "aes_profile:profile",
  "audio_lib:settings"
}

-- physics whilst in the lobby
hub.settings.physics = {
  speed = 1.5,
  speed_walk = 1,
  speed_fast = 1,
  speed_climb = 1,
  speed_crouch = 1,
  jump = 1,
  gravity = 1,
  liquid_fluidity = 1,
  liquid_fluidity_smooth = 1,
  liquid_sink = 1,
  acceleration_default = 1,
  acceleration_air = 1,
  acceleration_fast = 1,
  sneak = true,
  sneak_glitch = true,
  new_move = false,
}

-- commands that players won't be able to use whilst in the hub. Having the `hub_admin`
-- privilege bypasses the block
hub.settings.blocked_cmds = {
  "clearinv",
  "me",
  "pulverize"
}

-- tests to debug the mod
hub.settings.run_unit_tests = false



-----------------
---- METRICS ----
-----------------

-- assign every minigame to an ID. These IDs are then used to create a histogram through the monitoring mod.
-- To group more minigames in the same value, just use the same ID
hub.settings.metrics_minigames = {
  block_league = 1,
  fantasy_brawl = 2,
  quikbild = 3,
  murder = 4,
  skywars = 5,
  colour_jump = 6,
  sumo = 6,
  tntrun = 6,
  tnttag = 6,
  balloon_bop = 6
}

-- these arenas won't be taken in consideration when generating the histogram
hub.settings.metrics_exclude = {
  block_league = {Tutorial = true}
}



----------------
-- PLAYERLIST --
----------------

-- how many arenas to display in the arenas status panel (on the right)
hub.settings.MAX_ARENAS_IN_STATUS = 4

-- max slots in the playerlist table. If there are more players, it'll say "...and much more!" on the last slot
hub.settings.plist_max_slots = 36

-- max players per column in the playerlist table. When N +1 players are reached, a new column is generated, recalculating the position of the previous slots
hub.settings.plist_p_per_column = 12
